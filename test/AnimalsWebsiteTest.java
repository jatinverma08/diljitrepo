import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class AnimalsWebsiteTest {

	WebDriver driver;
	final String URL ="https://www.webdirectory.com/Animals/";
	
	final String Drivers_Path ="/Users/macstudent/Desktop/chromedriver";
	
	@Before
	public void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver",Drivers_Path);
		
		driver = new ChromeDriver();
		driver.get(URL);
		
	}

	@After
	public void tearDown() throws Exception {
		
		Thread.sleep(2000);
		driver.close();
	}

	@Test
	public void testNumberOfLinks() {
	
		List<WebElement> listOfLinks = driver.findElements(By.cssSelector("table+ul li a"));

		
		int numLinks = listOfLinks.size();
		assertEquals(10,numLinks);
	}

}
